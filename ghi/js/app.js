function createCard(name, description, pictureUrl, starts, ends,locationName) {
    return `
    
    <div class="shadow-lg bg-body rounded"
    <div class="card">
        <img src=${pictureUrl} class="card-img-top">
        <div class="card-body">
          <h4 class="card-title">${name}</h4>
          <h6 class="card-subtitle mb-3 text-muted">${locationName}</h6>
          <p class="card-text">${description}</p>
          
        
        
        </div>
        <div class="card-footer bg-transparent border-success">${new Date(starts).toLocaleDateString()} - ${new Date(ends).toLocaleDateString()}</div>
      </div>
      </div>
    `;
  }





  window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';
    try {
        const response = await fetch(url);

        if(!response.ok) {
            console.error('An error has occured');
        } else {
            const data = await response.json();
            const columns = document.querySelectorAll('.col');
            
            let columnIndex = 0;

            for (let conference of data.conferences) {
              const detailUrl = `http://localhost:8000${conference.href}`;
              const detailResponse = await fetch(detailUrl);
              if (detailResponse.ok) {
                const details = await detailResponse.json();
                const title = details.conference.name;
                const description = details.conference.description;
                const pictureUrl = details.conference.location.picture_url;
                const starts = details.conference.starts;
                const ends = details.conference.ends;
                const locationName = details.conference.location["name"]
                const html = createCard(title, description, pictureUrl,starts,ends,locationName);
                
                // const columns = document.querySelector('.col');
                // let columnIndex = 0;
                const column = columns[columnIndex];
                
                column.innerHTML += html;
                columnIndex = (columnIndex + 1) % columns.length
                console.log(details)

              }
            }

        }
            } catch (e) {
                throw new Error("Network response was not okay")
            }
          
          });

//     const url = 'http://localhost:8000/api/conferences/';
  
//     try {
//       const response = await fetch(url);
  
//       if (!response.ok) {
//         throw new Error("Network response was not okay")
//       } else {
//         const data = await response.json();
//         const columns = documents.querySelectorALL('.col');
//         console.log(columns);
//         let columnIndex =0
  
//         for (let conference of data.conferences) {
//           const detailUrl = `http://localhost:8000${conference.href}`;
//           const detailResponse = await fetch(detailUrl);
//           if (detailResponse.ok) {
//             const details = await detailResponse.json();
//             const name = details.conference.name;
//             const description = details.conference.description;
//             const pictureUrl = details.conference.location.picture_url;
//             const html = createCard(name, description, pictureUrl);
//             // const column = document.querySelector('.col');
//             const column =columns[columnIndex];
//             console.log(column)
//             column.innerHTML += html;
//             columnIndex = (columnIndex +1 ) % columns.length
//           }
//         }
  
//       }
//     } catch (e) {
//         throw new Error("Network response was not okay")
//     }
  
//   });



 
