import React, { useEffect,useState} from 'react';

function ConferenceForm () {
    const[locations,setLocations] = useState([]);
    const[name,setName] =useState('')
    const[start,setStart] =useState('')
    const[ends,setEnds] =useState('')
    const[description,setDescription] =useState('')
    const[presentation,setPresentation] =useState('')
    const[attendee,setAttendee] =useState('')
    const[location,setLocation] = useState('');
    

    const handleNameChange=(event) =>{
        const value = event.target.value;
        setName(value);
    }

    const startChange=(event) =>{
        const value = event.target.value;
        setStart(value);
    }


    const endsChange=(event) =>{
        const value = event.target.value;
        setEnds(value);
    }

    const descriptionChange=(event) =>{
        const value = event.target.value;
        setDescription(value);
    }

    const maxPresentationChange=(event) =>{
        const value = event.target.value;
        setPresentation(value);
    }

    const maxAttendeeChange=(event) =>{
        const value = event.target.value;
        setAttendee(value);
    }

    const handleLocationChange= (event) =>{
        const value = event.target.value;
        setLocation(value);
    }

    const fetchData = async ()=> {
        const url = 'http://localhost:8000/api/locations/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();

            setLocations(data.locations)
        }
    }

    useEffect(() => {
        fetchData();
      }, []);


    const handleSubmit= async (event)=>{
        event.preventDefault();
        const data ={};
        data.name = name;
        data.starts =start;
        data.ends=ends;
        data.description=description;
        data.max_presentations=presentation;
        data.max_attendees=attendee
        data.location=location
        console.log(data);
        
        const locationUrl = 'http://localhost:8000/api/conferences/' ;
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
            'Content-Type': 'application/json',
    },
  };

  const response = await fetch(locationUrl, fetchConfig);
  if (response.ok) {
    const newConference = await response.json();
    console.log(newConference);
    setName('');
    setStart('');
    setEnds('');
    setDescription('');
    setPresentation('');
    setAttendee('');
    setLocation('');
  }
    }


    
    return (
        <div className="container">
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Create a Conference</h1>
              <form onSubmit ={handleSubmit} id="create-conference-form">
                <div className="form-floating mb-3">
                  <input onChange ={handleNameChange} value= {name} placeholder="Name" required type="text" name = "name" id="name" className="form-control"/>
                  <label htmlFor="name">Name</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange ={startChange} value= {start}  type="date" required type="date"  id="starts" className="form-control"/>
                  <label htmlFor="starts">Starts</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange ={endsChange} value= {ends} type="date" required type="date" name = "ends" id="ends" className="form-control"/>
                  <label htmlFor="ends">Ends</label>
                </div>
                <div className="mb-3">
                  <label htmlFor="description" className="form-label">Description</label>
                  <textarea onChange ={descriptionChange} value= {description} className="form-control" name = "description" id="description" rows="3"></textarea>
                </div>
                <div className="form-floating mb-3">
                  <input onChange ={maxPresentationChange} value= {presentation} type="number" required type="number" name = "max_presentations" id="max_presentations"  className="form-control"/>
                  <label htmlFor="max_presentations">Maximum Presentation</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange ={maxAttendeeChange} value ={attendee} type="number" required type="number" name = "max_attendees" id="max_attendees" className="form-control"/>
                  <label htmlFor= "max_attendees">Maximum Attendee</label>
                </div>
                <div className="mb-3">
                  <select  onChange ={handleLocationChange} value = {location} required id="location" name = "location" className="form-select">
                    <option >Choose a location</option>
                    {locations.map(location => {
                        return (
                        <option key ={location.id} value={location.id}>
                            {location.name}
                        </option>
                        );
                    })}
                  </select>
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
}

export default ConferenceForm;
