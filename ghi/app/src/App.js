import {BrowserRouter,NavLink} from "react-router-dom";
import {Routes} from "react-router-dom"
import {Route} from "react-router-dom"

import Nav from './Nav';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import AttendConferenceForm from './AttendConferenceForm';
import ConferenceForm from './ConferenceForm';
import PresentationForm from "./PresentationForm";
import MainPage from "./MainPage";

function App(props) {
  if (props.attendees === undefined){
    return null;
  }
  return (
    <BrowserRouter>
      <Nav/>
      <div className = "container">
        <Routes>
          <Route>
            <Route index element ={<MainPage/>}/>        
            </Route>
          <Route path = 'locations'>
            <Route path = 'new' element ={<LocationForm/>}/>        
            </Route>
          <Route path = 'conferences'>
            <Route path = 'new' element ={<ConferenceForm/>}/>        
            </Route>
          <Route path = 'attendees'>
            <Route path = 'new' element ={<AttendConferenceForm/>}/>        
            </Route>
          <Route path = 'attendees'>
            <Route path = 'new' element ={<AttendeesList attendees = {props.attendees}/>} />        
            </Route>
          <Route path = 'presentation'>
            <Route path = 'new' element ={<PresentationForm/>} />        
            </Route>
        </Routes>
      {/* <AttendConferenceForm/> */}
      {/* <ConferenceForm/> */}
      {/* <LocationForm/> */}
      {/* <AttendeesList attendees = {props.attendees}/> */}
      </div>
    </BrowserRouter>
  );
}

export default App;
